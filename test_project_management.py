import gitlab
import unittest
import os

import project_management as pm 

private_token = os.environ['GITLAB_API_TOKEN']
salsa_ci_team = "Salsa CI Team"
test_repo_name = "init_unittesting_test_project"
test_project_url = "http://salsa.debian.org/xcancerberox-guest/" + test_repo_name


@unittest.skip
class TestInitProjectNotExists(unittest.TestCase):
    def setUp(self):
        self.cli = gitlab.Gitlab('https://salsa.debian.org', private_token=private_token)
        self.cli.auth()

    def test_get_not_existing_project_by_url(self):
        self.assertRaises(ValueError, pm.get_project_by_url, self.cli, test_project_url)


@unittest.skip
class TestInitProjectSetupEnv(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cli = gitlab.Gitlab('https://salsa.debian.org', private_token=private_token)
        cls.cli.auth()
        cls.test_project = cls.cli.projects.create({'name': test_repo_name, 'namespace': salsa_ci_team})

    @classmethod
    def tearDownClass(cls):
        cls.test_project.delete()

    def test_get_existing_project_by_url(self):
        project = pm.get_project_by_url(self.cli, test_project_url)
        self.assertEqual(project.name, test_repo_name)

    def test_create_init_branch(self):
        project = pm.get_project_by_url(self.cli, test_project_url)
        pm.create_init_branch(project, 'init_branch', 'master')
        branch_created = False
        for branch in project.branches.list():
            if branch.name == 'init_branch':
                branch_created = True
                break
        self.assertTrue(branch_created)


@unittest.skip
class TestInitProjectAndMerge(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.cli = gitlab.Gitlab('https://salsa.debian.org', private_token=private_token)
        cls.cli.auth()
        dummy_project = pm.get_project_by_url(cls.cli, 'https://salsa.debian.org/salsa-ci-team/dummypackage')
        dummy_project.forks.create({'name': test_repo_name, 'path': test_repo_name})
        cls.test_project = pm.get_project_by_url(cls.cli, 'https://salsa.debian.org/xcancerberox-guest/' + test_repo_name)

    @classmethod
    def tearDownClass(cls):
        cls.test_project.delete()

    def test_merge_on_pipeline_success(self):
        last_commit = pm.initialize_project(self.cli, self.test_project, ci_config_path='debian/test_ci_config.yml')
        pipeline = pm.wait_pipeline_creation_for(self.test_project, last_commit)
        status = pm.wait_pipeline_to_finish(self.test_project, pipeline)
        self.assertEqual(status, 'success')

        mr = pm.merge_initialized_branch(self.test_project, last_commit)
        self.assertEqual(mr.attributes['state'], 'merged')


class TestFillYML(unittest.TestCase):
    def test_fill_no_variables_on_default(self):
        self.assertTrue('variables' not in pm.fill_yml('unstable', False))

    def test_fill_variables_not_default_release(self):
        config = pm.fill_yml('stretch', False)
        self.assertTrue('variables' in config)
        self.assertTrue('RELEASE: stretch' in config)

    def test_fill_variables_not_free_deps(self):
        config = pm.fill_yml('unstable', True)
        self.assertTrue('variables' in config)
        self.assertTrue('SALSA_CI_COMPONENTS: main contrib non-free' in config)


if __name__ == '__main__':
    unittest.main()
